public class DCT
{
    int[][] block, trasformed, inv;

    public DCT(int[][] inputBlock)
    {
        this.block = inputBlock;
    }

    public int[][] shift()
    {
        for(int i = 0; i < 8; i++)
        {
            for(int j = 0; j < 8; j++)
            {
                block[i][j] = block[i][j] - 128;
            }
        }
        return block;
    }

    public int[][] shiftReverse()
    {
        for(int i = 0; i < 8; i++)
        {
            for(int j = 0; j < 8; j++)
            {
                inv[i][j] = inv[i][j] + 128;
            }
        }
        return inv;
    }

    public void transform()
    {
        trasformed = new int[8][8];
        double cosx, cosy, total, ci, cj;

        shift();

        for(int i = 0; i < 8; i++)
        {
            for(int j = 0; j < 8; j++)
            {
                total = 0.0;
                for(int x = 0; x < 8; x++)
                {
                    for(int y = 0; y < 8; y++)
                    {
                        cosx = (2.0 * (double)x + 1.0) * (double)i * Math.PI / 16.0;
                        cosy = (2.0 * (double)y + 1.0) * (double)j * Math.PI / 16.0;

                        total = total + ((double)block[x][y]) * Math.cos(cosx) * Math.cos(cosy);
                    }
                }

                if(i == 0)
                {
                    ci = 1.0 / Math.sqrt(2.0);
                }
                else
                {
                    ci = 1.0;
                }


                if(j == 0)
                {
                    cj = 1.0 / Math.sqrt(2.0);
                }
                else
                {
                    cj = 1.0;
                }

                total = total * ci * cj * 0.25;
                // TODO no round
//                trasformed[i][j] = (int)Math.round(total);
                trasformed[i][j] = (int) total;
            }
        }
    }

    public void inverse()
    {
        // https://github.com/JorenSix/TarsosDSP/blob/master/src/core/be/tarsos/dsp/mfcc/DCT.java
        inv = new int[8][8];

        for ( int x = 0; x < 8; x++ )
        {
            for ( int y = 0; y < 8; y++ )
            {
                double ge = 0.0;

                for ( int i = 0; i < 8; i++ )
                {
                    double cg1 = (2.0*(double)x + 1.0)*(double)i*Math.PI/16.0;
                    double ci = ((i==0)?1.0/Math.sqrt(2.0):1.0);

                    for ( int j = 0; j < 8; j++ )
                    {
                        double cg2 = (2.0*(double)y + 1.0)*(double)j*Math.PI/16.0;
                        double cj = ((j==0)?1.0/Math.sqrt(2.0):1.0);
                        double cij4 = ci*cj*0.25;
                        ge += cij4 * Math.cos(cg1) * Math.cos(cg2) * (double)block[i][j];
                    }
                }

                inv[x][y] = (int)Math.round(ge);
            }
        }

        inv = shiftReverse();
    }

    public int[][] getTrasformed() {
        return trasformed;
    }

    public int[][] getInv()
    {
        return inv;
    }
}
