import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;

public class Main
{
    public static void main(String[] args) throws IOException
    {
    	// -<d or e> <input> <output> <QF>
    	// example: -e Lena.raw Lena_encode 50
		// -e Baboon.raw Baboon_encode 50

		Main main = new Main();

		if(args.length < 3)
		{
			main.printInputFormat();
		}

    	else if(args[0].equals("-e"))
		{
			Encode encode = new Encode(args[2]);
			File file = new File(args[1]);
			byte[] inputFileBytes = Files.readAllBytes(file.toPath());
			int[][] temp;
			int[] temp1D;

			if(args.length != 4)
			{
				main.printInputFormat();
				System.exit(0);
			}

			System.out.println("\n*****\nprocessing ...");
			System.out.println("Encode [" + args[1] + "], Output [" + args[2] + "], QF = " + args[3]);

			// AC Table
			encode.generateHuffmanACTable();

			//splitBlock
			encode.splitToBlock(inputFileBytes);

			for(int i = 0; i < 4096; i++)
			{
//				encode.print2DMatrix(encode.getBlock(i));

				// DCT
				DCT dct = new DCT(encode.getBlock(i));
				dct.transform();
				temp = dct.getTrasformed();

				// Quantization
				temp = encode.quantization(temp, Parameters.getNewQuantum(Integer.parseInt(args[3]), Parameters.getQuantumLuminance()));

				// zig-zag
				temp1D = encode.zigZagScan(temp, Parameters.getZigzagOrder());

				// run-length
				ArrayList<RunLengthBlock> runLengthArray = encode.runLengthEncode(temp1D);

				// encoding
				encode.encoding(runLengthArray);
			}

			encode.close();
			System.out.println("\nDone.");
		}
		else if(args[0].equals("-d"))
		{
			String inputFile = args[1], outputFile = args[2];
			Decode decode = new Decode(inputFile, outputFile);
			int[][][] img;
			byte[] outputImg = new byte[4096 * 8 * 8];

			if(args.length != 4)
			{
				main.printInputFormat();
				System.exit(0);
			}

			System.out.println("\n*****\nprocessing ...");
			System.out.println("Decode [" + args[1] + "], Output [" + args[2] + "], QF = " + args[3]);

			// input file size
			File file = new File(inputFile);
			byte[] inputFileBytes = Files.readAllBytes(file.toPath());

			// build ac huffman tree
			decode.generateHuffmanACTable();
			decode.buildTree();

			// read test
			img = decode.decoding(inputFileBytes.length, Integer.parseInt(args[3]));
			outputImg = decode.reverseToImg(img);

			decode.output(outputImg);
			System.out.println("\nDone.");
		}
		else if(args[0].equals("-p"))
		{
			File origin = new File(args[1]);
			byte[] originFileBytes = Files.readAllBytes(origin.toPath());

			File after = new File(args[2]);
			byte[] afterFileBytes = Files.readAllBytes(after.toPath());

			PSNR psnr = new PSNR();
			double psnrOutput = psnr.calculate(originFileBytes, afterFileBytes);

			System.out.println("\nPSNR = [" + psnrOutput + "]");
		}
		else
		{
			System.out.println("input error");
		}
    }

    public void printInputFormat()
	{
		System.out.println();
		System.out.println("Wrong input format.");
		System.out.println();
		System.out.println("-------For encode & decode----------");
		System.out.println("-<d/e> <input> <output> <QF>");
		System.out.println();
		System.out.println("example:");
		System.out.println("-e Lena.raw Lena_encode 50");
		System.out.println("-d Lena_encode Lena_decoded.raw 50");
		System.out.println();
		System.out.println("--------------For PSNR--------------");
		System.out.println("-<p> <origin> <after>");
		System.out.println();
		System.out.println("example:");
		System.out.println("-p Lena.raw Lena_decoded.raw");
	}
}

class ACHuffmanTable
{
    ArrayList<ACHuffman> ACHuffmanMatrix;
    String[][] array = new String[16][11];
    
    
    public ACHuffmanTable()
    {
        this.ACHuffmanMatrix = new ArrayList<ACHuffman>();
    }

    public void addLineToACTable(String[] line)
    {
    	String tempRun, tempSize;
		tempRun = line[0].split("/")[0];
		tempSize = line[0].split("/")[1];

        ACHuffmanMatrix.add(new ACHuffman(
				Integer.parseInt(tempRun, 16),
				Integer.parseInt(tempSize,16),
        		line[1], 
        		line[2]));
        array[Integer.parseInt(tempRun, 16)][Integer.parseInt(tempSize, 16)] = line[2];
    }
    
    public String getACHuffmanCode(int run, int size)
    {
    	if(run >= 16 || size >= 11 || run < 0 || size < 0)
    	{
    		return "false";
    	}
    	
    	return array[run][size];
    }

    public void print()
    {
        for(int i = 0; i <ACHuffmanMatrix.size(); i++)
        {
            System.out.println(ACHuffmanMatrix.get(i));
        }

//		for(int i = 0; i < 16; i++)
//		{
//			for(int j = 0; j < 11; j++)
//			{
//				System.out.println("run = " + i + ", size = " + j + ", code= " +  array[i][j]);
//			}
//		}
    }

	public ArrayList<ACHuffman> getACHuffmanMatrix()
	{
		return ACHuffmanMatrix;
	}
}

class ACHuffman
{
	int run, category;
	String codeLength, code;

	//TODO 2D array ?
	public ACHuffman(int run, int category, String codeLength, String code)
	{
		this.run = run;
		this.category = category;
		this.codeLength = codeLength;
		this.code = code;
	}

	public int getRun()
	{
		return run;
	}

	public void setRun(int run)
	{
		this.run = run;
	}

	public int getCategory()
	{
		return category;
	}

	public void setCategory(int category)
	{
		this.category = category;
	}

	public String getCodeLength() 
	{
		return codeLength;
	}

	public void setCodeLength(String codeLength) 
	{
		this.codeLength = codeLength;
	}

	public String getCode() 
	{
		return code;
	}

	public void setCode(String code) 
	{
		this.code = code;
	}

	@Override
	public String toString() 
	{
		return "[run=" + run + ", "
				+ "category=" + category + ", "
				+ "codeLength=" + codeLength + ", "
				+ "code=" + code
				+ "]";
	}
}