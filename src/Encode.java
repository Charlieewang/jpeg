import java.io.*;
import java.nio.file.Files;
import java.util.ArrayList;

public class Encode
{
    int[][][] blocks;
    ACHuffmanTable acHuffmanTable = null;
    BitOutputStream outputStream = null;
    int DCpointer = 0;

    public static void main(String arg[]) throws IOException
    {
        // TODO arg[] input for filename & QF value
        Encode encode = new Encode("output");
        File file = new File("Baboon.raw");
        byte[] inputFileBytes = Files.readAllBytes(file.toPath());
        int[][] temp;
        int[] temp1D;

        // AC Table
        encode.generateHuffmanACTable();

        //splitBlock
        encode.splitToBlock(inputFileBytes);
//        encode.print2DMatrix(encode.getBlock(0));

        for(int i = 0; i < 4096; i++)
        {

//            encode.print2DMatrix(encode.getBlock(i));

            // DCT
            DCT dct = new DCT(encode.getBlock(i));
            dct.transform();
            temp = dct.getTrasformed();
//            encode.print2DMatrix(temp);
//            System.out.println();

            // Quantization
//            temp = encode.quantization(temp, Parameters.getQuantumLuminance()); // base table
            temp = encode.quantization(temp, Parameters.getNewQuantum(50, Parameters.getQuantumLuminance()));
//            encode.print2DMatrix(temp);
//            System.out.println();

            // zig-zag
            temp1D = encode.zigZagScan(temp, Parameters.getZigzagOrder());
//            encode.print1DMatrix(temp1D);
//            System.out.println();

            // run-length
            ArrayList<RunLengthBlock> runLengthArray = encode.runLengthEncode(temp1D);
            encode.printArrayList(runLengthArray);
        
            // encoding
            encode.encoding(runLengthArray);
//            System.out.println("----------[ block: " + i + " ]------------");
        }

        encode.close();
    }

    public Encode(String outputFilename) throws FileNotFoundException
    {
        // 64 blocks for 1 row
        blocks = new int[4096][8][8];
        acHuffmanTable = new ACHuffmanTable();
        outputStream = new BitOutputStream(new FileOutputStream(outputFilename));
    }

    public void splitToBlock(byte[] input)
    {
        int x = 0;

        for(int y = 0; y < 512; y = y + 8)
        {
            for (int i = 0; i < 512; i = i + 8)
            {
                for (int j = 0; j < 8; j++)
                { //row
                    for (int k = 0; k < 8; k++)
                    { //col
                        blocks[x][j][k] = input[ i + (y * 512) +  ( j*512 ) + k ] & 0xff;
                    }
                }
                x = x + 1;
            }
        }
    }

    public int[][] getBlock(int index)
    {
        int[][] temp = new int[8][8];

        if(index >= 4096)
        {
            return null;
        }

        for(int i = 0; i < 8; i++)
        {
            for(int j = 0; j < 8; j++)
            {
                temp[i][j] = blocks[index][i][j];
            }
        }

        return  temp;
    }

    public int[][] quantization(int[][] ori, int[] matrix)
    {
        int[][] temp = new int[8][8];

        for(int i = 0; i < 8; i++)
        {
            for (int j = 0; j < 8; j++)
            {
                temp[i][j] = Math.round((float) ori[i][j] / (float) matrix[i * 8 + j]);
            }
        }

        return temp;
    }

    public void print2DMatrix(int[][] input)
    {
        for(int i = 0; i < 8; i++)
        {
            for(int j = 0; j < 8; j++)
            {
                System.out.print(input[i][j]);
                System.out.print(", ");
            }
            System.out.println();
        }
    }

    public void print1DMatrix(int[] input)
    {

        for(int j = 0; j < input.length; j++)
        {
            System.out.print(input[j]);
            System.out.print(", ");
        }
        System.out.println();
    }

    public void printArrayList(ArrayList input)
    {

        for(int j = 0; j < input.size(); j++)
        {
            System.out.print(input.get(j));
            System.out.print(", ");
        }
        System.out.println();
    }

    public int[] zigZagScan(int[][] input, int[] order)
    {
        int[] zigZagOut = new int[64], temp = new int[64];

        //2D to 1D
        for(int i = 0; i < 8; i++)
        {
            for(int j = 0; j < 8; j++)
            {
                temp[i*8 + j] = input[i][j];
            }
        }

        for(int x = 0; x < 64; x++)
        {
            zigZagOut[x] = temp[order[x]];
        }

        return zigZagOut;
    }

    public ArrayList<RunLengthBlock> runLengthEncode(int[] input)
    {
        ArrayList<RunLengthBlock> runLengthBlocks = new ArrayList<RunLengthBlock>();
        int size = input.length - 1, zeroCount = 0;

        while(input[size] == 0 && size - 1 > 0)
        {
            size = size -  1;
        }

        for(int i = 0; i <= size; i++)
        {
            if(input[i] == 0)
            {
                if(zeroCount == 15)
                {
                    runLengthBlocks.add(new RunLengthBlock(zeroCount, "0"));
                    zeroCount = 0;
                }
                zeroCount = zeroCount + 1;
            }
            else
            {
                runLengthBlocks.add(new RunLengthBlock(zeroCount, Integer.toString(input[i])));
                zeroCount = 0;
            }
        }

        // TODO end of block
        runLengthBlocks.add(new RunLengthBlock(-1, "EOB"));

        return runLengthBlocks;
    }

    public void generateHuffmanACTable() throws IOException
    {
        String file = "ac_huffman_table.txt";
        FileReader fileReader = new FileReader(file);
        BufferedReader bufferedReader = new BufferedReader(fileReader);
        String line;

        // first line
        bufferedReader.readLine();

        // replace all space and tab, and split
        while((line = bufferedReader.readLine()) != null)
        {
            String[] test = line.split("\\s+");
            acHuffmanTable.addLineToACTable(test);
        }
//        acHuffmanTable.print();

        bufferedReader.close();
    }

    public void encoding(ArrayList<RunLengthBlock> list) throws IOException
    {
    	RunLengthBlock tempBlock = null;
    	int size, run, minusDC, ACValue;
    	
        // TODO DC值要先減去上一個block的DC, check 是量化後還是DCT後
    	// TODO ZRL, 0 超過 16 的例外處理

    	for(int i = 0; i < list.size(); i++)
    	{
    		tempBlock = list.get(i);

    		if(i == 0 && tempBlock.getNumberOfZero() != -1) // first -> DC
    		{
    			run = tempBlock.getNumberOfZero();
//                System.out.println("last DC = " + DCpointer);
                minusDC = Integer.parseInt(tempBlock.getValue()) - DCpointer;
        		size = Parameters.findCoefficientCategories(minusDC, true);
                DCpointer = Integer.parseInt(tempBlock.getValue());

//    			System.out.println(Parameters.getDcCodeWordLuminance(size) + ", DC = " + DCpointer + ", size = " + size + ", ");

    			output(Parameters.getDcCodeWordLuminance(size));
//                System.out.println((minusDC > 0) ? Integer.toBinaryString(minusDC) : Parameters.upsideBinary((-1) * minusDC));
    			output((minusDC >= 0) ? Integer.toBinaryString(minusDC) : Parameters.upsideBinary((-1) * minusDC));
    			// TODO add DC's DIFF value

    		}
    		else if(tempBlock.getNumberOfZero() != -1) // not EOB, AC
    		{
    			run = tempBlock.getNumberOfZero();
                ACValue = Integer.parseInt(tempBlock.getValue());
        		size = Parameters.findCoefficientCategories(ACValue, false);

//        		System.out.print("AC: ");
//    			System.out.print(acHuffmanTable.getACHuffmanCode(run, size));
    			output(acHuffmanTable.getACHuffmanCode(run, size));
    			// TODO jump ZRL
                if(run != 15 || size != 0)
                {
//                    System.out.print("/" + ACValue + " = ");
//                    System.out.println((ACValue > 0) ? Integer.toBinaryString(ACValue) : Parameters.upsideBinary((-1) * ACValue));
                    output((ACValue > 0) ? Integer.toBinaryString(ACValue) : Parameters.upsideBinary((-1) * ACValue));
                }
    		}
    		else
    		{
    		    if(i == 0)
                {
                    // if it has only EOB, output DC = 0
//                    System.out.println("last DC = " + DCpointer);
                    minusDC = 0 - DCpointer;
                    DCpointer = 0;
                    size = Parameters.findCoefficientCategories(minusDC, true);
//                    System.out.println(Parameters.getDcCodeWordLuminance(size) + ", DC = " + DCpointer + ", size = " + size + ", ");
                    output(Parameters.getDcCodeWordLuminance(size));
//                    System.out.println((minusDC > 0) ? Integer.toBinaryString(minusDC) : Parameters.upsideBinary((-1) * minusDC));
                    output((minusDC >= 0) ? Integer.toBinaryString(minusDC) : Parameters.upsideBinary((-1) * minusDC));
                }
//    			else if(tempBlock.getNumberOfZero() == -1)
    			// EOB
//    			System.out.println(1010 + ", [EOB]");
    			output("1010");
    		}
    	}
    }
    
    public void output(String input) throws IOException
    {
    	for (int i = 0; i < input.length(); i++) 
		{
	    	outputStream.write(input.charAt(i) == '1');
		}
    }
    
    public void close() throws IOException
    {
    	outputStream.close();
    }
}

class RunLengthBlock
{
    int numberOfZero;
    String value;

    public RunLengthBlock(int run, String value)
    {
        this.numberOfZero = run;
        this.value = value;
    }

    public int getNumberOfZero() 
    {
		return numberOfZero;
	}

	public void setNumberOfZero(int numberOfZero) 
	{
		this.numberOfZero = numberOfZero;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@Override
    public String toString()
    {
        return "<" + numberOfZero + ", " + value + ">" ;
    }
}
