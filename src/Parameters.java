public class Parameters
{

    // base
    static final int[] QUANTUM_LUMINANCE =
            { 16,  11,  10,  16,  24,  40,  51,  61,
            12,  12,  14,  19,  26,  58,  60,  55,
            14,  13,  16,  24,  40,  57,  69,  56,
            14,  17,  22,  29,  51,  87,  80,  62,
            18,  22,  37,  56,  68, 109, 103,  77,
            24,  36,  55,  64,  81, 104, 113,  92,
            49,  64,  78,  87, 103, 121, 120, 101,
            72,  92,  95,  98, 112, 100, 103,  99};

    static final int[] QUANTUM_CHROMINANCE =
            { 17, 18, 24, 47, 99, 99, 99, 99,
            18, 21, 26, 66, 99, 99, 99, 99,
            24, 26, 56, 99, 99, 99, 99, 99,
            47, 66, 99, 99, 99, 99, 99, 99,
            99, 99, 99, 99, 99, 99, 99, 99,
            99, 99, 99, 99, 99, 99, 99, 99,
            99, 99, 99, 99, 99, 99, 99, 99,
            99, 99, 99, 99, 99, 99, 99, 99};

    static final int[] ZIGZAG_ORDER =
            { 0,  1,  8,  16,  9,  2,  3, 10,
            17, 24, 32, 25, 18, 11,  4,  5,
            12, 19, 26, 33, 40, 48, 41, 34,
            27, 20, 13,  6,  7, 14, 21, 28,
            35, 42, 49, 56, 57, 50, 43, 36,
            29, 22, 15, 23, 30, 37, 44, 51,
            58, 59, 52, 45, 38, 31, 39, 46,
            53, 60, 61, 54, 47, 55, 62, 63};

    static final String[] DC_CODE_WORD_LUMINANCE =
            { "00", "010", "011", "100", "101", "110", "1110", "11110", "111110", "1111110", "11111110", "111111110"};

    static final String[] DC_CODE_WORD_CHROMINANCE =
            { "00", "01", "10", "110", "1110", "11110", "111110", "1111110", "11111110", "111111110", "1111111110", "11111111110"};

    public static int[] getQuantumLuminance()
    {
        return QUANTUM_LUMINANCE;
    }

    public static int getS(int Q)
    {
        int S = 0;

        if(Q < 50)
        {
            S = 5000 / Q;
        }
        else
        {
            S = 200 - 2 * Q;
        }

        return S;
    }

    //TODO test
    public static int[] getNewQuantum(int Q, int[] QUANTUM)
    {
        int S = getS(Q);
        int[] newQuantum = new int[64];

        for(int i = 0; i < newQuantum.length; i++)
        {
            newQuantum[i] = (S * QUANTUM[i] + 50) / 100;
            if(newQuantum[i] == 0)
            {
                newQuantum[i] = 1;
            }
        }


        return newQuantum;
    }

    public static String getDcCodeWordLuminance(int i)
    {
        return DC_CODE_WORD_LUMINANCE[i];
    }

    public static String getDcCodeWordChrominance(int i)
    {
        return DC_CODE_WORD_CHROMINANCE[i];
    }

    public static int[] getQuantumChrominance()
    {
        return QUANTUM_CHROMINANCE;
    }

    public static int[] getZigzagOrder()
    {
        return ZIGZAG_ORDER;
    }

    public static int findCoefficientCategories(int input, boolean type)
    {
        int n = -1, length = 0;

        if(input < 0)
        {
            input = -(input);
        }

        // true for DC
        if(type == true)
        {
            length = 12;
        }
        else
        {
            length = 11;
        }

        for(int i = 0; i < length; i++)
        {
            if( type == true && input == 0)
            {
                return 0;
            }
            else if(input >= Math.pow(2, i) && input < Math.pow(2, i + 1))
            {
                n = i + 1;
            }

        }
        return n;

    }


    public static String upsideBinary(int input)
    {
        String out = "";
        String in = Integer.toBinaryString(input);

        for(int i = 0; i < in.length(); i++)
        {
            if(in.charAt(i) == '0')
            {
                out = out + '1';
            }
            else
            {
                out = out + '0';
            }
        }

        return out;
    }

    public static String upsideBinaryForString(String in)
    {
        String out = "";

        for(int i = 0; i < in.length(); i++)
        {
            if(in.charAt(i) == '0')
            {
                out = out + '1';
            }
            else
            {
                out = out + '0';
            }
        }

        return out;
    }

    public static int getDCCategory(String code, String[] DCTable)
    {
        for(int i = 0; i < DCTable.length; i++)
        {
            if(code.equals(DCTable[i]))
            {
                return i;
            }
        }

        return -1;
    }
}
