import java.io.*;
import java.nio.file.Files;
import java.util.ArrayList;

public class Decode
{
    ACHuffmanTable acHuffmanTable = null;
    BinaryTree binaryTree = null;
    BitInputStream in = null;
    FileOutputStream out = null;
    BitOutputStream outputStream = null;
    int DCPointer = 0;

    public static void main(String arg[]) throws IOException
    {
        String inputFile = "output", outputFile = "decoded-baboon.raw";
        Decode decode = new Decode(inputFile, outputFile);
        int[][][] img;
        byte[] outputImg = new byte[4096 * 8 * 8];

        // input file size
        File file = new File(inputFile);
        byte[] inputFileBytes = Files.readAllBytes(file.toPath());

        // build ac huffman tree
        decode.generateHuffmanACTable();
        decode.buildTree();

        // read test
        img = decode.decoding(inputFileBytes.length, 50);
        decode.print2DMatrix(decode.getBlock(0, img));
        outputImg = decode.reverseToImg(img);

        decode.output(outputImg);

    }

    public Decode(String input, String output) throws FileNotFoundException
    {
        acHuffmanTable = new ACHuffmanTable();
        binaryTree = new BinaryTree();

        in = new BitInputStream(new BufferedInputStream(new FileInputStream(input)));
        out = new FileOutputStream(output);

    }

    public void output(byte[] output) throws IOException
    {
        out.write(output);
    }

    public int[][] getBlock(int index, int[][][] blocks)
    {
        int[][] temp = new int[8][8];

        if(index >= 4096)
        {
            return null;
        }

        for(int i = 0; i < 8; i++)
        {
            for(int j = 0; j < 8; j++)
            {
                temp[i][j] = blocks[index][i][j];
            }
        }

        return  temp;
    }

    public String readBit() throws IOException
    {
        if(in.read())
        {
            return "1";
        }
        else
        {
            return "0";
        }
    }

    public int[][][] decoding(int filesize, int QF) throws IOException
    {
        int[][][] img = new int[4096][8][8];
        int[][] block = new int[8][8];
        String temp = "", dc;
        ArrayList<RunLengthBlock> runLengthBlocks = new ArrayList<>();
        int dcCategory, count = 0;
        int[] acArray;
        boolean DCflag = true;
        DCT dct;

        while(count < 4096)
        {
            temp = temp + readBit();
//            System.out.println("temp: " + temp);

            if(DCflag)
            {
                if((dcCategory = searchDC(temp)) != -1)
                {
                    DCflag = false;
//                    System.out.println("DC category = " + dcCategory);
                    temp = "";

                    if(dcCategory == 0)
                    {
                        dc = readBit();
                        temp = "";
                        DCPointer = Integer.parseInt(dc) + DCPointer;
                        runLengthBlocks.add(new RunLengthBlock(0, Integer.toString(DCPointer)));
                    }
                    else
                    {
                        for (int j = 0; j < dcCategory; j++)
                        {
                            temp = temp + readBit();
                        }
                        // TODO DC table reverse

                        if (temp.charAt(0) == '0')
                        {
                            DCPointer = (-1) * Integer.parseInt(Parameters.upsideBinaryForString(temp), 2) + DCPointer;
                        }
                        else
                        {
                            DCPointer = Integer.parseInt(temp, 2) + DCPointer;
                        }

//                        System.out.println("DC = " + DCPointer);
                        runLengthBlocks.add(new RunLengthBlock(0, Integer.toString(DCPointer)));
                        temp = "";
                    }
                    // TODO clear
                }
            }
            else
            {
                if((acArray = searchACTree(temp)) != null)
                {
                    if(temp.equals("1010")) //EOB
                    {
                        temp = "";
                        DCflag = true;
                        runLengthBlocks.add(new RunLengthBlock(-1, "EOB")); // EOB = <0, EOB>

                        // TODO output 8*8 to img
//                        printArrayList(runLengthBlocks);
//                        print1DMatrix(runLengthBlocksToZigZagList(runLengthBlocks));
//                        print1DMatrix(zigZagReverse(runLengthBlocksToZigZagList(runLengthBlocks), Parameters.getZigzagOrder()));
                        block = transfer1Dto2D(zigZagReverse(runLengthBlocksToZigZagList(runLengthBlocks), Parameters.getZigzagOrder()));
                        block = quantizationReverse(block, Parameters.getNewQuantum(QF, Parameters.getQuantumLuminance()));
//                        System.out.println("-------------------------");
                        dct = new DCT(block);
                        dct.inverse();
                        block = dct.getInv();
//                        print2DMatrix(dct.getInv());

                        for (int j = 0; j < 8; j++)
                        { //row
                            for (int k = 0; k < 8; k++)
                            { //col
                                img[count][j][k] = block[j][k];
                            }
                        }
                        count = count + 1;
//                        System.out.println("-----------EOB------------, [count = " + count + " ]");

                        runLengthBlocks = new ArrayList<>();
                    }
                    else if(acArray[0] == 15 && acArray[1] == 0) //ZRL
                    {
//                        System.out.println("-----------ZRL------------");
                        temp = "";
                        runLengthBlocks.add(new RunLengthBlock(acArray[0], Integer.toString(acArray[1]))); // ZRL = <15, 0>
                    }
                    else
                    {
//                        System.out.println("AC <run, length> =  <" + acArray[0] + ", " + acArray[1] + ">");
                        temp = "";
                        for (int j = 0; j < acArray[1]; j++)
                        {
                            temp = temp + readBit();
                        }

//                        System.out.println("ac temp: " + temp);

                        if (temp.charAt(0) == '0')
                        {
                            // TODO 整理
//                            System.out.println("ac decode[0]: <" + acArray[0] + ", " + (-1) * Integer.parseInt(Parameters.upsideBinaryForString(temp), 2) + ">");
                            runLengthBlocks.add(new RunLengthBlock(acArray[0], Integer.toString((-1) * Integer.parseInt(Parameters.upsideBinaryForString(temp), 2))));
                        }
                        else
                        {
//                            System.out.println("ac decode[1]: <" + acArray[0] + ", " + Integer.parseInt(temp, 2) + ">");
                            runLengthBlocks.add(new RunLengthBlock(acArray[0], Integer.toString(Integer.parseInt(temp, 2))));
                        }
                        temp = "";
                    }
                }
            }
        }

        return img;
    }

    public byte[] reverseToImg(int[][][] input)
    {
        int x = 0;
        byte[] out = new byte[4096 * 8 * 8];

        for(int y = 0; y < 512; y = y + 8)
        {
            for (int i = 0; i < 512; i = i + 8)
            {
                for (int j = 0; j < 8; j++)
                { //row
                    for (int k = 0; k < 8; k++)
                    { //col
                        out[ i + (y * 512) +  ( j * 512 ) + k ] = (byte) input[x][j][k];
                    }
                }
                x = x + 1;
            }
        }

        return out;
    }

    public int[] runLengthBlocksToZigZagList(ArrayList<RunLengthBlock> inputBlock)
    {
        int count = 0;
        int[] out = new int[64];
        RunLengthBlock temp;
        for(int i = 0; i < inputBlock.size(); i++)
        {
            temp = inputBlock.get(i);
            if(temp.getNumberOfZero() == -1) // EOB
            {
                for(int j = count; j < 64; j++)
                {
                    out[j] = 0;
                }
            }
            else if(temp.getNumberOfZero() == 15 && temp.getValue().equals("0")) // ZRL
            {
                for(int j = 0; j < temp.getNumberOfZero(); j++)
                {
                    out[j + count] = 0;
                }
                count = count + temp.getNumberOfZero();
            }
            else
            {
                for(int j = 0; j < temp.getNumberOfZero(); j++)
                {
                    out[j + count] = 0;
                }
                count = count + temp.getNumberOfZero();
                out[count] = Integer.parseInt(temp.getValue());
                count = count + 1;

            }
        }

        return out;
    }

    public int[] zigZagReverse(int[] input, int[] order)
    {
        int[] zigZagOut = new int[64];

        for(int x = 0; x < 64; x++)
        {
            zigZagOut[order[x]] = input[x];
        }

        return zigZagOut;
    }

    public int[][] quantizationReverse(int[][] ori, int[] matrix)
    {
        int[][] temp = new int[8][8];

        for(int i = 0; i < 8; i++)
        {
            for (int j = 0; j < 8; j++)
            {
                temp[i][j] = Math.round((float) ori[i][j] * (float) matrix[i * 8 + j]);
            }
        }

        return temp;
    }


    public int[][] transfer1Dto2D(int[] input)
    {
        int length = input.length;
        int[][] out = new int[8][8];

        for(int i = 0; i < 8; i ++)
        {
            for(int j = 0; j < 8 / 2; j++)
            {
                out[i][j] = input[i * 8 + j];
            }
        }

        return out;
    }

    public void printArrayList(ArrayList input)
    {

        for(int j = 0; j < input.size(); j++)
        {
            System.out.print(input.get(j));
            System.out.print(", ");
        }
        System.out.println();
    }


    public void generateHuffmanACTable() throws IOException
    {
        String file = "ac_huffman_table.txt";
        FileReader fileReader = new FileReader(file);
        BufferedReader bufferedReader = new BufferedReader(fileReader);
        String line;

        // first line
        bufferedReader.readLine();

        // replace all space and tab, and split
        while((line = bufferedReader.readLine()) != null)
        {
            String[] test = line.split("\\s+");
            acHuffmanTable.addLineToACTable(test);
        }
//        acHuffmanTable.print();

        bufferedReader.close();
    }

    public void buildTree()
    {
        binaryTree.build(acHuffmanTable.getACHuffmanMatrix());
    }
    public int[] searchACTree(String in)
    { // if not found, null
        return binaryTree.search(in);
    }

    public int searchDC(String in)
    { // if not found, return -1
        return Parameters.getDCCategory(in, Parameters.DC_CODE_WORD_LUMINANCE);
    }

    public void print1DMatrix(int[] input)
    {

        for(int j = 0; j < input.length; j++)
        {
            System.out.print(input[j]);
            System.out.print(", ");
        }
        System.out.println();
    }

    public void print2DMatrix(int[][] input)
    {
        for(int i = 0; i < 8; i++)
        {
            for(int j = 0; j < 8; j++)
            {
                System.out.print(input[i][j]);
                System.out.print(", ");
            }
            System.out.println();
        }
    }

}

class BinaryTree
{
    Node root, pointer;

    public BinaryTree()
    {
        root = new Node();
        pointer = root;
    }

    public void build(ArrayList<ACHuffman> arrayList)
    {
        for(int i = 0; i < arrayList.size(); i++)
        {
            addNode(arrayList.get(i));
        }
    }

    public void addNode(ACHuffman acHuffman)
    {
        int run, size;
        String code;

        run = acHuffman.getRun();
        size = acHuffman.getCategory();
        code = acHuffman.getCode();

        for(int i =  0; i < code.length(); i++)
        {
            if(code.charAt(i) == '0')
            {
                if(pointer.hasLeftNode())
                {
                    pointer = pointer.getLeftNode();
//                    System.out.print("0");
                }
                else
                {
                    Node node = new Node();
                    pointer.addLeftNode(node);
                    pointer = pointer.getLeftNode();
//                    System.out.print("0");
                }
            }
            else
            {
                if(pointer.hasRightNode())
                {
                    pointer = pointer.getRightNode();
//                    System.out.print("1");
                }
                else
                {
                    Node node = new Node();
                    pointer.addRightNode(node);
                    pointer = pointer.getRightNode();
//                    System.out.print("1");
                }
            }
        }
        pointer.setLeaf(run, size);
        pointer = root;
//        System.out.println(", out");
    }

    public int[] search(String code)
    {
        int[] node = new int[2];

        pointer = root;
        for(int i =  0; i < code.length(); i++)
        {
            if (code.charAt(i) == '0')
            {
                pointer = pointer.getLeftNode();
            }
            else
            {
                pointer = pointer.getRightNode();
            }
        }

        if(pointer.isLeaf())
        {
//            System.out.println(pointer.getRun() + ", " + pointer.getSize());
            node[0] = pointer.getRun();
            node[1] = pointer.getSize();
            return node;
        }
        else
        {
//            System.out.println("search wrong");
            return null;
        }
    }


}

class Node
{
    private boolean isLeaf = false;
    private int run, size;
    private Node leftNode, rightNode;

    public Node()
    {
        this.isLeaf = false;
        this.rightNode = null;
        this.leftNode = null;
        this.run = -1;
        this.size = -1;
    }

    public Node(Node left, Node right)
    {
        this.isLeaf = false;
        this.rightNode = null;
        this.leftNode = null;
        this.leftNode = left;
        this.rightNode = right;
    }

    public Node(int run, int size)
    {
        this.isLeaf = true;
        this.run = run;
        this.size = size;
    }

    public void addRightNode(Node node)
    {
        if(!hasRightNode())
        {
            this.rightNode = node;
        }
    }

    public void addLeftNode(Node node)
    {
        if(!hasLeftNode())
        {
            this.leftNode = node;
        }
    }

    public boolean hasRightNode()
    {
        return this.rightNode != null;
    }

    public boolean hasLeftNode()
    {
        return this.leftNode != null;
    }

    public void setLeaf(int run, int size)
    {
        this.isLeaf = true;
        this.run = run;
        this.size = size;
    }

    public int getRun()
    {
        return run;
    }

    public int getSize()
    {
        return size;
    }

    public boolean isLeaf()
    {
        return isLeaf;
    }

    public Node getLeftNode()
    {
        return leftNode;
    }

    public Node getRightNode()
    {
        return rightNode;
    }
}