import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

public class PSNR
{
    public double calculate(byte[] origin, byte[] after)
    {
        double MSE = 0, PSNR = 0;
        int total = 0, sub = 0;

        for(int i = 0; i < 512 * 512; i++)
        {
            sub =  (origin[i] & 0xff) - (after[i] & 0xff);
            total = total + (int) Math.pow(sub, 2);
        }

        MSE = (float) total / (512 * 512);
        PSNR = 10 * Math.log10((255.0 * 255.0) / MSE);

        return PSNR;
    }
}
